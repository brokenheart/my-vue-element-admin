# vue-admin-template

## ⛱️预览

> [一睹为快](http://brokenheart.gitee.io/my-vue-element-admin/#/demo/index)

## 模板来源以下项目

* [vue-element-admin](https://gitee.com/panjiachen/vue-element-admin)
* [vue-admin-template](https://gitee.com/panjiachen/vue-admin-template)

## 🌈 介绍
 基于[element-ui](https://element.eleme.io/#/zh-CN/component/installation)做了以下组件的简单封装，开发起来更快捷

* BoxLayout `上中下布局`
* MyButton 和[el-button](https://element.eleme.io/#/zh-CN/component/button)用法一样
* MyCascader 和[el-cascader](https://element.eleme.io/#/zh-CN/component/cascader)用法基本一样
* MyCheckbox 对[el-checkbox](https://element.eleme.io/#/zh-CN/component/checkbox)进行了封装
* MyDialog
* MyDrawer
* MyForm
* MyIcon
* MyLink
* MyPagination
* MyPopconfirm
* MyPopover
* MyRadio
* MyRate
* MySelect
* MyTable
* MyTabs
* MyTooltip

###  🚧 使用方式

```javascript
// 下拉代码
git clone https://gitee.com/brokenheart/my-vue-element-admin.git
// 切换分支
git checkout dev-cms
// 安装依赖
npm install
//启动
npm run dev
// 访问
http://localhost:9528 // 当然，端口你也可以自己配置
// 打包
npm run build:prod
```

### 变动较大的有

- BoxLayout
- MyDialog
- MyDrawer
- MyForm
- MyPagination
- MyTable

## 基本展示

* ![基本展示页](./show-imgs/展示页.jpg)

* ![my-table与分页](./show-imgs/table与分页.jpg)

* ![全选与导出](./show-imgs/全选及导出.jpg)

* ![my-dialog](./show-imgs/mydialog弹窗.jpg)

* ![my-drawer橱窗](./show-imgs/橱窗.jpg)

* ![my-icon](./show-imgs/my-icon.jpg)