import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/zh-CN' // lang i18n

import '@/styles/index.scss' // global css

// my components
import components from '@/utils/components.js'
Vue.use(components)

import App from './App'
import store from './store'
import router from './router'

import directives from '@/directive'
Vue.use(directives)

import '@/icons' // icon
import '@/permission' // permission control

import { copyContent } from '@/utils/methods'
import msgs from '@/utils/message'

Vue.prototype.$myCopyClip = copyContent
for (const key in msgs) {
  Vue.prototype[`$${key}`] = msgs[key]
}
/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online ! ! !
 */
// if (process.env.NODE_ENV === 'production') {
//   const { mockXHR } = require('../mock')
//   mockXHR()
// }

// set ElementUI lang to EN
Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
// Vue.use(ElementUI)

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
