module.exports = {
  /**
  * 侧边栏主题 深色主题theme-dark，浅色主题theme-light
  */
  sideTheme: 'theme-dark',

  /**
   * 是否系统布局配置
   */
  showSettings: false,

  title: 'Vue Admin Template',
  /**
 * @type {boolean} true | false
 * @description Whether need tagsView
 */
  tagsView: true,

  /**
   * @type {boolean} true | false
   * @description Whether fix the header
   */
  fixedHeader: true,

  /**
   * @type {boolean} true | false
   * @description Whether show the logo in sidebar
   */
  sidebarLogo: false
}
