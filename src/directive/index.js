import elDragDialog from './elDragDialog'

const directives = [
  elDragDialog
]

export default (Vue) => {
  directives.forEach(directive => {
    Vue.use(directive)
  })
}
