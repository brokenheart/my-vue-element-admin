import { login, logout, getInfo } from '@/api/user'
import { getToken, setToken, removeToken } from '@/utils/auth'
import { resetRouter } from '@/router'

const localUser = JSON.parse(localStorage.getItem('localUser') || '{}')

const getDefaultState = () => {
  return {
    token: getToken(),
    name: localUser.name || '',
    avatar: localUser.avatar || ''
  }
}

const state = getDefaultState()

const storeLocalUser = (state) => {
  localStorage.setItem('localUser', JSON.stringify(state))
}

const mutations = {
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
    storeLocalUser(state)
  },
  SET_TOKEN: (state, token) => {
    state.token = token
    storeLocalUser(state)
  },
  SET_NAME: (state, name) => {
    state.name = name
    storeLocalUser(state)
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
    storeLocalUser(state)
  }
}

const actions = {
  // user login
  login({ commit }, userInfo) {
    const { username, password } = userInfo
    return new Promise((resolve, reject) => {
      login({ username: username.trim(), password: password }).then(response => {
        const { data } = response
        commit('SET_TOKEN', data.token)
        setToken(data.token)
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  setAvatar({ commit }, state) {
    commit('SET_AVATAR', state.avatar)
  },

  // get user info
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      getInfo(state.token).then(response => {
        const { data } = response

        if (!data) {
          return reject('Verification failed, please Login again.')
        }

        const { name, avatar } = data

        commit('SET_NAME', name)
        commit('SET_AVATAR', avatar)
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // user logout
  logout({ commit, state }) {
    return new Promise((resolve, reject) => {
      logout(state.token).then(() => {
        removeToken() // must remove  token  first
        resetRouter()
        commit('RESET_STATE')
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      removeToken() // must remove  token  first
      commit('RESET_STATE')
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

