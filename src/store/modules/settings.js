import variables from '@/styles/variables.scss'
import defaultSettings from '@/settings'

const { showSettings, fixedHeader, sidebarLogo, tagsView } = defaultSettings

const localSetting = JSON.parse(localStorage.getItem('localSetting') || '{}')

function setDialogHeader(theme) {
  if (document.querySelector('.el-dialog__header')) {
    document.querySelector('.el-dialog__header').style.setProperty('background-color', theme)
  }
  if (document.querySelector('.el-drawer__header')) { document.querySelector('.el-drawer__header').style.setProperty('background-color', theme) }
}

const state = {
  theme: localSetting.theme || variables.colorPrimary,
  sideTheme: localSetting.theme || variables.colorPrimary,
  tagsView: [undefined, null, 'undefined', 'null'].includes(localSetting.tagsView) ? tagsView : localSetting.tagsView,
  fixedHeader: localSetting.fixedHeader || fixedHeader,
  sidebarLogo: [undefined, null, 'undefined', 'null'].includes(localSetting.sidebarLogo) ? sidebarLogo : localSetting.sidebarLogo,
  showSettings: [undefined, null, 'undefined', 'null'].includes(localSetting.showSettings) ? showSettings : localSetting.showSettings
}

const mutations = {
  CHANGE_SETTING: (state, { key, value }) => {
    console.log('key: ', key)
    // eslint-disable-next-line no-prototype-builtins
    if (state.hasOwnProperty(key)) {
      state[key] = value
      localStorage.setItem('localSetting', JSON.stringify(state))
    }
    if (key === 'theme' && !!value) {
      setDialogHeader(value)
    }
  },
  RESET_SETTING: (state) => {
    localStorage.setItem('localSetting', '{}')
    const sourceState = {
      theme: variables.colorPrimary,
      sideTheme: variables.colorPrimary,
      tagsView: tagsView,
      fixedHeader: fixedHeader,
      sidebarLogo: sidebarLogo,
      showSettings: showSettings
    }
    for (const key in sourceState) {
      !!state[key] && (state[key] = sourceState[key])
    }
    if (variables.them) {
      setDialogHeader(variables.theme)
    }
  }
}

const actions = {
  changeSetting({ commit }, data) {
    commit('CHANGE_SETTING', data)
  },
  resetSetting({ commit }) {
    commit('RESET_SETTING')
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

