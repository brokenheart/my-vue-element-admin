import svgIcons from './svg-icon'
export default {
  data() {
    const tableData = new Array(100).fill(null, 0, 100).map((v, i) => {
      return { userName: `张三${i + 1}`, lastLoginTime: `${new Date().toLocaleDateString()}`, phone: `182733${i + 1}3323`, email: `123@1${i + 1}3.com`, status: 1, gend: 2 }
    })
    return {
      columns: [
        { type: 'expand', slotName: 'expandSlot' },
        { type: 'selection' },
        { type: 'index' },
        { label: '用户昵称', prop: 'userName', width: 120 },
        { label: '最近登录时间', prop: 'lastLoginTime' },
        // { label: '用户头像', prop: 'vatar' },
        { label: '联系方式', prop: 'phone', width: 240 },
        { label: '邮箱', prop: 'email', width: 200 },
        { label: '性别', prop: 'gend', width: 120, type: 'slot' },
        { label: '状态', prop: 'status', type: 'slot' },
        { type: 'action', label: '操作' }
      ],
      tableData,
      pageInfo: {
        pageSize: 15,
        pageNum: 1,
        total: tableData.length
      },
      multipleSelection: [],
      exportLoading: false,

      itemsDatas: [
        { label: '用户昵称', prop: 'userName', type: 'input' },
        { label: '联系方式', prop: 'phone', type: 'input' },
        { label: '邮箱', prop: 'email', type: 'input' },
        { label: '状态', prop: 'status', type: 'select', clearable: true, options: [{ label: '禁封', value: -1 }, { label: '正常', value: 1 }] },
        { label: '性别', prop: 'gend', type: 'select', clearable: true, options: [{ label: '男', value: 2 }, { label: '女', value: 1 }] }
      ],
      formDatas: {
        userName: '',
        phone: '',
        email: '',
        status: 1,
        gend: 2
      },
      rules: {
        userName: [{ required: true, message: '请输入用户昵称', trigger: 'blur' }]
      },
      addFormDatas: [],
      addItemsDatas: {},
      addRules: {},

      options: [{
        value: 'zhinan',
        label: '指南',
        children: [{
          value: 'shejiyuanze',
          label: '设计原则',
          children: [{
            value: 'yizhi',
            label: '一致'
          }, {
            value: 'fankui',
            label: '反馈',
            disabled: true
          }, {
            value: 'xiaolv',
            label: '效率'
          }, {
            value: 'kekong',
            label: '可控'
          }]
        }, {
          value: 'daohang',
          label: '导航',
          children: [{
            value: 'cexiangdaohang',
            label: '侧向导航'
          }, {
            value: 'dingbudaohang',
            label: '顶部导航'
          }]
        }]
      }, {
        value: 'zujian',
        label: '组件',
        children: [{
          value: 'basic',
          label: 'Basic',
          children: [{
            value: 'layout',
            label: 'Layout 布局'
          }, {
            value: 'color',
            label: 'Color 色彩'
          }, {
            value: 'typography',
            label: 'Typography 字体'
          }, {
            value: 'icon',
            label: 'Icon 图标'
          }, {
            value: 'button',
            label: 'Button 按钮'
          }]
        }, {
          value: 'form',
          label: 'Form',
          children: [{
            value: 'radio',
            label: 'Radio 单选框'
          }, {
            value: 'checkbox',
            label: 'Checkbox 多选框'
          }, {
            value: 'input',
            label: 'Input 输入框'
          }, {
            value: 'input-number',
            label: 'InputNumber 计数器'
          }, {
            value: 'select',
            label: 'Select 选择器'
          }, {
            value: 'cascader',
            label: 'Cascader 级联选择器'
          }, {
            value: 'switch',
            label: 'Switch 开关'
          }, {
            value: 'slider',
            label: 'Slider 滑块'
          }, {
            value: 'time-picker',
            label: 'TimePicker 时间选择器'
          }, {
            value: 'date-picker',
            label: 'DatePicker 日期选择器'
          }, {
            value: 'datetime-picker',
            label: 'DateTimePicker 日期时间选择器'
          }, {
            value: 'upload',
            label: 'Upload 上传'
          }, {
            value: 'rate',
            label: 'Rate 评分'
          }, {
            value: 'form',
            label: 'Form 表单'
          }]
        }, {
          value: 'data',
          label: 'Data',
          children: [{
            value: 'table',
            label: 'Table 表格'
          }, {
            value: 'tag',
            label: 'Tag 标签'
          }, {
            value: 'progress',
            label: 'Progress 进度条'
          }, {
            value: 'tree',
            label: 'Tree 树形控件'
          }, {
            value: 'pagination',
            label: 'Pagination 分页'
          }, {
            value: 'badge',
            label: 'Badge 标记'
          }]
        }, {
          value: 'notice',
          label: 'Notice',
          children: [{
            value: 'alert',
            label: 'Alert 警告'
          }, {
            value: 'loading',
            label: 'Loading 加载'
          }, {
            value: 'message',
            label: 'Message 消息提示'
          }, {
            value: 'message-box',
            label: 'MessageBox 弹框'
          }, {
            value: 'notification',
            label: 'Notification 通知'
          }]
        }, {
          value: 'navigation',
          label: 'Navigation',
          children: [{
            value: 'menu',
            label: 'NavMenu 导航菜单'
          }, {
            value: 'tabs',
            label: 'Tabs 标签页'
          }, {
            value: 'breadcrumb',
            label: 'Breadcrumb 面包屑'
          }, {
            value: 'dropdown',
            label: 'Dropdown 下拉菜单'
          }, {
            value: 'steps',
            label: 'Steps 步骤条'
          }]
        }, {
          value: 'others',
          label: 'Others',
          children: [{
            value: 'dialog',
            label: 'Dialog 对话框'
          }, {
            value: 'tooltip',
            label: 'Tooltip 文字提示'
          }, {
            value: 'popover',
            label: 'Popover 弹出框'
          }, {
            value: 'card',
            label: 'Card 卡片'
          }, {
            value: 'carousel',
            label: 'Carousel 走马灯'
          }, {
            value: 'collapse',
            label: 'Collapse 折叠面板'
          }]
        }]
      }, {
        value: 'ziyuan',
        label: '资源',
        children: [{
          value: 'axure',
          label: 'Axure Components'
        }, {
          value: 'sketch',
          label: 'Sketch Templates'
        }, {
          value: 'jiaohu',
          label: '组件交互文档'
        }]
      }],

      drawerVisiable: false,

      iconList: [
        'sugar',
        'ice-cream-round',
        'ice-cream-square',
        'lollipop',
        'potato-strips',
        'milk-tea',
        'ice-drink',
        'ice-tea',
        'coffee',
        'orange',
        'pear',
        'apple',
        'cherry',
        'watermelon',
        'grape',
        'refrigerator',
        'goblet-square-full',
        'goblet-square',
        'goblet-full',
        'goblet',
        'cold-drink',
        'coffee-cup',
        'water-cup',
        'hot-water',
        'ice-cream',
        'dessert',
        'tableware',
        'burger',
        'knife-fork',
        'fork-spoon',
        'chicken',
        'food',
        'dish-1',
        'dish',
        'moon-night',
        'moon',
        'cloudy-and-sunny',
        'partly-cloudy',
        'cloudy',
        'sunny',
        'sunset',
        'sunrise-1',
        'sunrise',
        'heavy-rain',
        'lightning',
        'light-rain',
        'wind-power',
        'baseball',
        'soccer',
        'football',
        'basketball',
        'ship',

        'truck',

        'bicycle',

        'mobile-phone',

        'service',

        'key',

        'unlock',

        'lock',

        'watch',

        'watch-1',

        'timer',

        'alarm-clock',

        'map-location',

        'delete-location',

        'add-location',

        'location-information',

        'location-outline',

        'location',

        'place',

        'discover',

        'first-aid-kit',

        'trophy-1',

        'trophy',

        'medal',

        'medal-1',

        'stopwatch',

        'mic',

        'copy-document',

        'full-screen',

        'switch-button',

        'aim',

        'crop',

        'odometer',

        'time',

        'bangzhu',

        'close-notification',

        'microphone',

        'turn-off-microphone',

        'position',

        'postcard',

        'message',

        'chat-line-square',

        'chat-dot-square',

        'chat-dot-round',

        'chat-square',

        'chat-line-round',

        'chat-round',

        'set-up',

        'turn-off',

        'open',

        'connection',

        'link',

        'cpu',

        'thumb',

        'female',

        'male',

        'guide',

        'news',

        'price-tag',

        'discount',

        'wallet',

        'coin',

        'money',

        'bank-card',

        'box',

        'present',

        'sell',

        'sold-out',

        'shopping-bag-2',

        'shopping-bag-1',

        'shopping-cart-2',

        'shopping-cart-1',

        'shopping-cart-full',

        'smoking',

        'no-smoking',

        'house',

        'table-lamp',

        'school',

        'office-building',

        'toilet-paper',

        'notebook-2',

        'notebook-1',

        'files',

        'collection',

        'receiving',

        'suitcase-1',

        'suitcase',

        'film',

        'collection-tag',

        'data-analysis',

        'pie-chart',

        'data-board',

        'data-line',

        'reading',

        'magic-stick',

        'coordinate',

        'mouse',

        'brush',

        'headset',

        'umbrella',

        'scissors',

        'mobile',

        'attract',

        'monitor',

        'search',

        'takeaway-box',

        'paperclip',

        'printer',

        'document-add',

        'document',

        'document-checked',

        'document-copy',

        'document-delete',

        'document-remove',

        'tickets',

        'folder-checked',

        'folder-delete',

        'folder-remove',

        'folder-add',

        'folder-opened',

        'folder',

        'edit-outline',

        'edit',

        'date',

        'c-scale-to-original',

        'view',

        'rank',

        'sort-down',

        'sort-up',

        'sort',

        'finished',

        'refresh-left',

        'refresh-right',

        'refresh',

        'video-play',

        'video-pause',

        'd-arrow-right',

        'd-arrow-left',

        'arrow-up',

        'arrow-down',

        'arrow-right',

        'arrow-left',

        'top-right',

        'top-left',

        'top',

        'bottom',

        'right',

        'back',

        'bottom-right',

        'bottom-left',

        'caret-top',

        'caret-bottom',

        'caret-right',

        'caret-left',

        'd-caret',

        'share',

        'menu',

        's-grid',

        's-check',

        's-data',

        's-opportunity',

        's-custom',

        's-claim',

        's-finance',

        's-comment',

        's-flag',

        's-marketing',

        's-shop',

        's-open',

        's-management',

        's-ticket',

        's-release',

        's-home',

        's-promotion',

        's-operation',

        's-unfold',

        's-fold',

        's-platform',

        's-order',

        's-cooperation',

        'bell',

        'message-solid',

        'video-camera',

        'video-camera-solid',

        'camera',

        'camera-solid',

        'download',

        'upload2',

        'upload',

        'picture-outline-round',

        'picture-outline',

        'picture',

        'close',

        'check',

        'plus',

        'minus',

        'help',

        's-help',

        'circle-close',

        'circle-check',

        'circle-plus-outline',

        'remove-outline',

        'zoom-out',

        'zoom-in',

        'error',

        'success',

        'circle-plus',

        'remove',

        'info',

        'question',

        'warning-outline',

        'warning',

        'goods',

        's-goods',

        'star-off',

        'star-on',

        'more-outline',

        'more',

        'phone-outline',

        'phone',

        'user',

        'user-solid',

        'setting',

        's-tools',

        'delete',

        'delete-solid',

        'eleme',
        'platform-eleme',
        'loading'
      ],
      svgIcons
    }
  }
}
