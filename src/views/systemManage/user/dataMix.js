export default {
  data() {
    const tableData = new Array(100).fill(null, 0, 100).map((v, i) => {
      return { userName: `张三${i + 1}`, phone: `182733${i + 1}3323`, salary: Math.ceil(Math.random() * 10000), email: `123@1${i + 1}3.com`, status: 1, gend: Math.ceil(Math.random() * 2), fourLabel: '4级' + Math.random() * 100 }
      // return { userName: `张三${i + 1}`, phone: `182733${i + 1}3323`, salary: Math.ceil(Math.random() * 10000), email: `123@1${i + 1}3.com`, status: 1, gend: Math.ceil(Math.random() * 2) }
    })
    return {
      columns: [
        { type: 'selection' },
        { label: '编号', type: 'index' },
        { label: '用户昵称', prop: 'userName', width: 120 },
        { label: '用户头像', prop: 'vatar', type: 'slot' },
        { label: '二级', prop: 'HeaderTest', align: 'center', subColumns: [
          { label: '最近登录时间', prop: 'lastLoginTime', type: 'slot' },
          { label: '薪资', prop: 'salary', sum: true },
          { label: '联系方式', prop: 'phone', width: 240 },
          { label: '邮箱', prop: 'email', width: 120 },
          { label: '三级', subColumns: [
            { label: '三级1', prop: 'threeTest1' },
            { label: '三级2', prop: 'threeTest', subColumns: [
              { label: '四级', prop: 'fourLabel' },
              { label: '四级2', prop: 'fourLabel2' }
            ] }
          ] }
        ] },
        { label: 'eje', subColumns: [
          { label: 'test', prop: 'test' },
          { label: '性别', prop: 'gend', width: 120, type: 'slot' }
        ] },
        { label: '状态', prop: 'status', width: 120, type: 'slot' },
        { type: 'action', label: '操作' }
      ],
      tableData,
      pageInfo: {
        pageSize: 15,
        pageNum: 1,
        total: tableData.length
      },
      multipleSelection: [],

      addVisiable: false,

      itemsDatas: [
        { label: '用户昵称', prop: 'userName', type: 'input' },
        { label: '联系方式', prop: 'phone', type: 'input' },
        { label: '邮箱', prop: 'email', type: 'input' },
        { label: '状态', prop: 'status', type: 'select', clearable: true, options: [{ label: '禁封', value: -1 }, { label: '正常', value: 1 }] },
        { label: '性别', prop: 'gend', type: 'select', clearable: true, options: [{ label: '男', value: 2 }, { label: '女', value: 1 }] }
      ],
      formDatas: {
        userName: '',
        phone: '',
        email: '',
        status: 1,
        gend: 2
      },
      rules: {
        userName: [{ required: true, message: '请输入用户昵称', trigger: 'blur' }]
      },
      addFormDatas: [],
      addItemsDatas: {},
      addRules: {},

      drawerVisiable: false,

      iconList: [
        'sugar',
        'ice-cream-round',
        'ice-cream-square',

        'lollipop',

        'potato-strips',

        'milk-tea',

        'ice-drink',

        'ice-tea',

        'coffee',

        'orange',

        'pear',

        'apple',

        'cherry',

        'watermelon',

        'grape',

        'refrigerator',

        'goblet-square-full',

        'goblet-square',

        'goblet-full',

        'goblet',

        'cold-drink',

        'coffee-cup',

        'water-cup',

        'hot-water',

        'ice-cream',

        'dessert',

        'tableware',

        'burger',

        'knife-fork',

        'fork-spoon',

        'chicken',

        'food',

        'dish-1',

        'dish',

        'moon-night',

        'moon',

        'cloudy-and-sunny',

        'partly-cloudy',

        'cloudy',

        'sunny',

        'sunset',

        'sunrise-1',

        'sunrise',

        'heavy-rain',

        'lightning',

        'light-rain',

        'wind-power',

        'baseball',

        'soccer',

        'football',

        'basketball',

        'ship',

        'truck',

        'bicycle',

        'mobile-phone',

        'service',

        'key',

        'unlock',

        'lock',

        'watch',

        'watch-1',

        'timer',

        'alarm-clock',

        'map-location',

        'delete-location',

        'add-location',

        'location-information',

        'location-outline',

        'location',

        'place',

        'discover',

        'first-aid-kit',

        'trophy-1',

        'trophy',

        'medal',

        'medal-1',

        'stopwatch',

        'mic',

        'copy-document',

        'full-screen',

        'switch-button',

        'aim',

        'crop',

        'odometer',

        'time',

        'bangzhu',

        'close-notification',

        'microphone',

        'turn-off-microphone',

        'position',

        'postcard',

        'message',

        'chat-line-square',

        'chat-dot-square',

        'chat-dot-round',

        'chat-square',

        'chat-line-round',

        'chat-round',

        'set-up',

        'turn-off',

        'open',

        'connection',

        'link',

        'cpu',

        'thumb',

        'female',

        'male',

        'guide',

        'news',

        'price-tag',

        'discount',

        'wallet',

        'coin',

        'money',

        'bank-card',

        'box',

        'present',

        'sell',

        'sold-out',

        'shopping-bag-2',

        'shopping-bag-1',

        'shopping-cart-2',

        'shopping-cart-1',

        'shopping-cart-full',

        'smoking',

        'no-smoking',

        'house',

        'table-lamp',

        'school',

        'office-building',

        'toilet-paper',

        'notebook-2',

        'notebook-1',

        'files',

        'collection',

        'receiving',

        'suitcase-1',

        'suitcase',

        'film',

        'collection-tag',

        'data-analysis',

        'pie-chart',

        'data-board',

        'data-line',

        'reading',

        'magic-stick',

        'coordinate',

        'mouse',

        'brush',

        'headset',

        'umbrella',

        'scissors',

        'mobile',

        'attract',

        'monitor',

        'search',

        'takeaway-box',

        'paperclip',

        'printer',

        'document-add',

        'document',

        'document-checked',

        'document-copy',

        'document-delete',

        'document-remove',

        'tickets',

        'folder-checked',

        'folder-delete',

        'folder-remove',

        'folder-add',

        'folder-opened',

        'folder',

        'edit-outline',

        'edit',

        'date',

        'c-scale-to-original',

        'view',

        'rank',

        'sort-down',

        'sort-up',

        'sort',

        'finished',

        'refresh-left',

        'refresh-right',

        'refresh',

        'video-play',

        'video-pause',

        'd-arrow-right',

        'd-arrow-left',

        'arrow-up',

        'arrow-down',

        'arrow-right',

        'arrow-left',

        'top-right',

        'top-left',

        'top',

        'bottom',

        'right',

        'back',

        'bottom-right',

        'bottom-left',

        'caret-top',

        'caret-bottom',

        'caret-right',

        'caret-left',

        'd-caret',

        'share',

        'menu',

        's-grid',

        's-check',

        's-data',

        's-opportunity',

        's-custom',

        's-claim',

        's-finance',

        's-comment',

        's-flag',

        's-marketing',

        's-shop',

        's-open',

        's-management',

        's-ticket',

        's-release',

        's-home',

        's-promotion',

        's-operation',

        's-unfold',

        's-fold',

        's-platform',

        's-order',

        's-cooperation',

        'bell',

        'message-solid',

        'video-camera',

        'video-camera-solid',

        'camera',

        'camera-solid',

        'download',

        'upload2',

        'upload',

        'picture-outline-round',

        'picture-outline',

        'picture',

        'close',

        'check',

        'plus',

        'minus',

        'help',

        's-help',

        'circle-close',

        'circle-check',

        'circle-plus-outline',

        'remove-outline',

        'zoom-out',

        'zoom-in',

        'error',

        'success',

        'circle-plus',

        'remove',

        'info',

        'question',

        'warning-outline',

        'warning',

        'goods',

        's-goods',

        'star-off',

        'star-on',

        'more-outline',

        'more',

        'phone-outline',

        'phone',

        'user',

        'user-solid',

        'setting',

        's-tools',

        'delete',

        'delete-solid',

        'eleme',
        'platform-eleme',
        'loading',
        '-right',
        '-left'
      ]

    }
  }
}
