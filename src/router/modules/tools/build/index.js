export default [
  {
    path: '/build/index',
    component: () => import('@/views/tools/build/index'), // Parent router-view,
    name: 'build',
    meta: { title: '可视化构建工具' }
  }
]
