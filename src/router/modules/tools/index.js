/* Layout */
import Layout from '@/layout'

import build from './build'

export default {
  path: '/tools',
  component: Layout,
  alwaysShow: true, // will always show the root menu
  name: 'toolsManage',
  meta: { title: '工具管理', icon: 'nested' },
  children: [...build]
}
