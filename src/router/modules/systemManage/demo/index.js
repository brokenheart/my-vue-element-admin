
export default [
  {
    path: '/demo/index',
    component: () => import('@/views/systemManage/demo/index'), // Parent router-view,
    name: 'demo',
    meta: { title: '查询及展示' }
  }
]
