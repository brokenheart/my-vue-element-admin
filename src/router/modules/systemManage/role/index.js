/* Layout */
// import Layout from '@/layout'

export default [
  {
    path: '/role/index',
    component: () => import('@/views/systemManage/role/index'), // Parent router-view,
    name: 'role',
    meta: { title: '角色管理' }
  }
]
