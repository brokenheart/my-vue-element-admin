/* Layout */
import Layout from '@/layout'

import userManage from './user'
import roleManage from './role'
import demoManage from './demo'

export default {
  path: '/systemManage',
  component: Layout,
  alwaysShow: true, // will always show the root menu
  name: 'systemManage',
  meta: { title: '系统管理', icon: 'nested' },
  children: [...userManage, ...roleManage, ...demoManage]
}
