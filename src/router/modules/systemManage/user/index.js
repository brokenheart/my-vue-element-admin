/* Layout */
// import Layout from '@/layout'

export default [
  {
    path: '/user/index',
    component: () => import('@/views/systemManage/user/index'), // Parent router-view,
    name: 'user',
    meta: { title: '用户管理' }
  },
  {
    path: '/user/profile/index',
    component: () => import('@/views/systemManage/user/profile/index'), // Parent router-view,
    name: 'profile',
    meta: { title: '头像管理' }
  }
]
