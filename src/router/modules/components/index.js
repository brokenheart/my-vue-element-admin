import Layout from '@/layout'
export default {
  path: '/components',
  component: Layout,
  meta: { title: 'components', icon: 'form' },
  alwaysShow: true, // will always show the root menu
  children: [
    {
      path: 'boxLayout',
      name: 'boxLayout',
      component: () => import('@/views/components/boxLayout'),
      meta: { title: '上中下布局', icon: '' }
    },
    {
      path: 'myForm',
      name: 'MyForm',
      component: () => import('@/views/components/form'),
      meta: { title: '表单', icon: '' }
    },
    {
      path: 'myTable',
      name: 'MyTable',
      component: () => import('@/views/components/table'),
      meta: { title: '表格', icon: '' }
    },
    {
      path: 'myDialog',
      name: 'myDialog',
      component: () => import('@/views/components/dialog'),
      meta: { title: '弹窗与橱窗', icon: '' }
    },
    {
      path: 'myIcon',
      name: 'myIcon',
      component: () => import('@/views/components/icon'),
      meta: { title: '图标', icon: '' }
    },
    {
      path: 'myUpload',
      name: 'myUpload',
      component: () => import('@/views/components/upload'),
      meta: { title: '文件上传', icon: '' }
    },
    {
      path: 'myEditor',
      name: 'myEditor',
      component: () => import('@/views/components/wangEditor'),
      meta: { title: '富文本编辑器', icon: '' }
    }

  ]
}
