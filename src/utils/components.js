
import MyForm from '@/components/Form'
import MyFormItem from '@/components/Form/FormItem'
import MyTable from '@/components/Table'
import MyPagination from '@/components/Pagination'
import MyDialog from '@/components/Dialog'
import MyDrawer from '@/components/Drawer'
import MyTabs from '@/components/Tabs'
import MyLink from '@/components/Link'
import MyButtonGroup from '@/components/Button/ButtonGroup.vue'
import MyButton from '@/components/Button'
import MyRadio from '@/components/Radio'
import MyRadioGroup from '@/components/Radio/RadioGroup.vue'
import MyCheckBox from '@/components/Checkbox'
import MySelect from '@/components/Select'
import MyCascader from '@/components/Cascader'

import MyIcon from '@/components/Icon'
import MyRate from '@/components/Rate'

import MyTooltip from '@/components/Tooltip'
import MyPopover from '@/components/Popover'
import MyPopconfirm from '@/components/Popconfirm'

import boxLayout from '@/components/Box'
import BoxTitle from '@/components/Box/BoxTitle'
import MyUpload from '@/components/Upload'
import MyEditor from '@/components/Editor'

const components = [
  MyForm,
  MyTable,
  MyFormItem,
  MyPagination,
  MyDialog,
  MyDrawer,
  MyTabs,
  MyLink,
  MyButtonGroup,
  MyButton,
  MyCheckBox,
  MyRadio,
  MyRadioGroup,
  MySelect,
  MyCascader,

  MyIcon,
  MyRate,

  MyTooltip,
  MyPopover,
  MyPopconfirm,

  BoxTitle,
  boxLayout,
  MyUpload,
  MyEditor

]

export default (Vue) => {
  components.forEach(component => {
    Vue.component(component.name, component)
  })
}
