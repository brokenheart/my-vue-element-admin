import { trigger } from './config'

let confGlobal
let someSpanIsNot24

export function vueTemplate(str) {
  return `<template>
    <div>
      ${str}
    </div>
  </template>`
}

export function vueScript(str) {
  return `<script>
    ${str}
  </script>`
}

function colWrapper(element, str) {
  if (someSpanIsNot24 || element.span !== 24) {
    return `<el-col :span="${element.span}">${str}</el-col>`
  }
  return str
}

const layouts = {
  colFormItem(element) {
    let labelWidth = ''
    if (element.labelWidth && element.labelWidth !== confGlobal.labelWidth) {
      labelWidth = `label-width='${element.labelWidth}px'`
    }
    const required = !trigger[element.tag] && element.required ? 'required' : ''
    const tagDom = tags[element.tag] ? tags[element.tag](element) : null
    let str = `<el-form-item ${labelWidth} label="${element.label}" prop="${element.vModel}" ${required}>
      ${tagDom}
    </el-form-item>`
    str = colWrapper(element, str)
    return str
  },
  rowFormItem(element) {

  }
}

const tags = {
  'el-button': el => {
    const { disabled } = attrBuilder(el)
    const type = el.type ? `type="${el.type}"` : ''
    const icon = el.icon ? `icon="${el.icon}"` : ''
    const size = el.size ? `size="${el.size}"` : ''
    let child = buildElButtonChild(el)

    if (child) child = `\n${child}\n`
    return `<${el.tag} ${type} ${icon} ${size} ${disabled}>${child}</${el.tag}>`
  },
  'el-input': el => {
    const { disabled, vModel, clearable, placeholder, width } = attrBuilder(el)
    const maxlength = el.maxlength ? `maxlength='${el.maxlength}'` : ''
    const showWordLimit = el['show-word-limit'] ? 'show-word-limit' : ''
    const readonly = el.readonly ? 'readonly' : ''
    const prefixIcon = el['prefix-icon'] ? `prefix-icon=${el['prefix-icon']}` : ''
    const suffixIcon = el['suffix-icon'] ? `suffix-icon=${el['suffix-icon']}` : ''
    const showPassword = el['show-password'] ? `show-passowrd` : ''
    const type = el.type ? `type='${el.type}'` : ''
    const autoSize = el.autoSize && el.autoSize.minRows ? `:autoSize="{minRows: ${el.autoSize.minRows}, maxRows: ${el.autoSize.maxRows}}"` : ''

    let child = buildElInputChild(el)
    if (child) child = `\n${child}\n`
    return `<${el.tag} ${vModel} ${clearable} ${placeholder} ${disabled} ${maxlength} ${type} ${prefixIcon} ${suffixIcon} ${showPassword} ${readonly} ${showWordLimit} ${autoSize} ${width}>${child}</${el.tag}>`
  },
  'el-input-number': el => {
    const { disabled, vModel, clearable, placeholder } = attrBuilder(el)
    const controlsPosition = el['controls-position'] ? `controls-position=${el['controls-position']}` : ''
    const max = el['max'] ? `:max='${el.max}'` : ''
    const min = el['min'] ? `:min='${el.min}'` : ''
    const step = el['step'] ? `:step='${el.step}'` : ''
    const stepStrictly = el['step-strictly'] ? `step-strictly` : ''
    const precision = el.precision ? `:precision='${el.precision}'` : ''

    return `<${el.tag} ${vModel} ${clearable} ${placeholder} ${disabled} ${min} ${max} ${controlsPosition} ${step} ${precision} ${stepStrictly} />`
  },
  'el-select': el => {
    const { disabled, vModel, clearable, placeholder, width } = attrBuilder(el)
    const filterable = el.filterable ? `filterable` : ''
    const multiple = el.multiple ? 'multiple' : ''
    let child = buildElSelectChild(el)

    if (child) child = `\n${child}\n`
    return `<${el.tag} ${vModel} ${clearable} ${placeholder} ${disabled} ${filterable} ${multiple} ${width}>${child}</${el.tag}>`
  },
  'el-cascader': el => {
    const { disabled, vModel, clearable, placeholder, width } = attrBuilder(el)
    const options = el.options ? `:options=${el.vModel}Options` : ''
    const props = el.props ? `:props=${el.vModel}Props` : ''
    const showAllLevels = el['show-all-levels'] ? '' : ':show-all-levels="false"'
    const filterable = el.filterable ? 'filterable' : ''
    const separator = el.separator === '/' ? '' : `separator="${el.separator}"`

    return `<${el.tag}${vModel} ${options} ${props} ${width} ${showAllLevels} ${placeholder} ${separator} ${filterable} ${clearable} ${disabled}></${el.tag}>`
  },
  'el-radio-group': el => {
    const { disabled, vModel } = attrBuilder(el)
    const size = `size="${el.size}"`
    let child = buildElRadioGroupChild(el)

    if (child) child = `\n${child}\n`
    return `<${el.tag} ${vModel} ${disabled} ${size}>${child}</${el.tag}>`
  },
  'el-checkbox-group': el => {
    const { disabled, vModel } = attrBuilder(el)
    const size = `size=${el.size}`
    let child = buildElCheckboxGroupChild(el)

    if (child) child = `\n${child}\n`
    return `<${el.tag} ${vModel} ${disabled} ${size}>${child}</${el.tag}>`
  },
  'el-switch': el => {
    const { disabled, vModel } = attrBuilder(el)
    const activeText = el['active-text'] ? `active-text="${el['active-text']}"` : ''
    const inactiveText = el['inactive-text'] ? `inactive-text="${el['inactive-text']}"` : ''
    const activeColor = el['active-color'] ? `active-color="${el['active-color']}"` : ''
    const inactiveColor = el['inactive-color'] ? `inactive-color="${el['inactive-color']}"` : ''
    const activeValue = el['active-value'] !== true ? `:active-value="${JSON.stringify(el['active-value'])}"` : ''
    const inactiveValue = el['inactive-value'] !== false ? `:inactive-value="${JSON.stringify(el['inactive-value'])}"` : ''

    return `<${el.tag} ${vModel} ${activeText} ${inactiveText} ${activeColor} ${inactiveColor} ${activeValue} ${inactiveValue} ${disabled}></${el.tag}>`
  },
  'el-slider': el => {
    const { disabled, vModel } = attrBuilder(el)
    const min = el.min ? `:min="${el.min}"` : ''
    const max = el.max ? `:max="${el.max}"` : ''
    const step = el.step ? `:step="${el.step}"` : ''
    const range = el.range ? 'range' : ''
    const showStops = el['show-stops'] ? `:show-stops="${el['show-stops']}"` : ''

    return `<${el.tag} ${min} ${max} ${step} ${vModel} ${range} ${showStops} ${disabled}></${el.tag}>`
  },
  'el-time-picker': {

  },
  'el-date-picker': {

  },
  'el-rate': {

  },
  'el-color-picker': {

  },
  'el-upload': {

  }
}

function attrBuilder(el) {
  return {
    vModel: `v-model='${confGlobal.formModel}.${el.vModel}'`,
    clearable: el.clearable ? 'clearable' : '',
    placeholder: el.placeholder ? `placeholder='${el.placeholder}'` : '',
    width: el.style && el.style.width ? `:style="{width: \'100%\'}"` : '',
    disabled: el.disabled ? `:disabled=\'true\'` : ''
  }
}

function buildElButtonChild(conf) {
  const children = []
  if (conf.default) {
    children.push(conf.default)
  }
  return children.join('\n')
}

function buildElInputChild(conf) {
  const children = []
  if (conf.prepend) {
    children.push(`<template slot="prepend">${conf.prepend}</template>`)
  }
  if (conf.append) {
    children.push(`<template slot="append">${conf.append}</template>`)
  }
  return children.join('\n')
}

function buildElSelectChild(conf) {
  const children = []
  if (conf.options && conf.options.length) {
    children.push(`<el-option v-for="(v,i) in ${conf.vModel}Options" :key="i" :label="v.label" :value="v.value" :disabled="v.disabled"`)
  }
  return children.join('\n')
}

function buildElRadioGroupChild(conf) {
  const children = []
  if (conf.options && conf.options.length) {
    const tag = conf.optionType === 'button' ? `optionType="el-radio-button"` : `el-radio`
    const border = conf.border ? 'border' : ''
    children.push(`<${tag} v-for="(v, i) in ${conf.vModel}Options" :key="i" :label="v.label" :value="v.value" :disabled="v.disabled" ${border}>{{item.label}}</${tag}>`)
  }
  return children.join('\n')
}

function buildElCheckboxGroupChild(conf) {
  const children = []
  if (conf.options && conf.options.length) {
    const tag = conf.optionType === 'button' ? `optionType="el-checkbox-button"` : `el-checkbox`
    const border = conf.border ? 'border' : ''
    children.push(`<${tag} v-for="(v, i) in ${conf.vModel}Options" :key="i" :label="v.label" :value="v.value" :disabled="v.disabled" ${border}>{{v.label}}</${tag}>`)
  }
  return children.join('\n')
}

function buildFormTemplate(conf, htmlStr, type) {
  const disabled = conf.disabled ? `:disabled="${conf.disabled}"` : ''
  let str = `<el-form ref="${conf.formRef}" :model="${conf.formModel}" :rules="${conf.formRules}" size="${conf.size}" labelPosition="${conf.labelPosition}" labelWidth="${conf.labelWidth}px" ${disabled}>${htmlStr}</el-form>`
  if (conf.formBtns) {
    str += `<el-col :span="24" style="text-align: right">
      <el-button @click="resetForm">重置</el-button>
      <el-button type="primary" @click="submitForm">确定</el-button>
    </el-col>`
  }
  if (someSpanIsNot24) {
    str = `<el-row :gutter="${conf.gutter}">
      ${str}
    </el-row>`
  }
  return str
}

export function makeUpHtml(conf, type) {
  const htmlList = []
  confGlobal = conf
  someSpanIsNot24 = conf.fields.some(item => item.span !== 24)
  conf.fields.forEach(el => {
    htmlList.push(layouts[el.layout](el))
  })
  return buildFormTemplate(conf, htmlList.join('\n'), type)
}
