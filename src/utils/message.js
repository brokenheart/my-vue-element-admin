import { Message, MessageBox, Notification } from 'element-ui'
const msgInfo = (message = 'info', showClose = true, center = false, duration = 3000, ...args) => {
  Message({
    showClose,
    message,
    center,
    duration,
    args,
    type: 'info'
  })
}

const msgSuccess = (message = 'success', showClose = true, center = false, duration = 3000, ...args) => {
  return Message({
    showClose,
    message,
    center,
    duration,
    args,
    type: 'success'
  })
}

const msgWarn = (message = 'warning', showClose = true, center = false, duration = 3000, ...args) => {
  return Message({
    showClose,
    message,
    center,
    duration,
    args,
    type: 'warning'
  })
}
const msgError = (message = 'error', showClose = true, center = false, duration = 3000, ...args) => {
  return Message({
    showClose,
    message,
    center,
    duration,
    args,
    type: 'error'
  })
}

const msgHtml = (html = '<strong>这是 <i>HTML</i> 片段</strong>', showClose = true, center = false, duration = 3000, ...args) => {
  return Message({
    showClose,
    dangerouslyUseHTMLString: true,
    message: html,
    center,
    args,
    duration
  })
}

const msgConfirm = (message = '确定此操作?', title = '提示', type = 'warning', showCancelButton = true, ...options) => {
  return new Promise((resolve, reject) => {
    MessageBox.confirm(message, title, {
      confirmButtonText: '确定',
      cancelButtonText: '取消',
      showCancelButton,
      type,
      options
    }).then(res => {
      resolve(res)
    }).catch(err => {
      reject(err)
    })
  })
}

const notifySuccess = (message = 'success', title = '成功', ...args) => {
  return Notification({
    title,
    message,
    type: 'success',
    ...args
  })
}

const notifyWarn = (message = 'warning', title = '警告', ...args) => {
  return Notification({
    title,
    message,
    type: 'warning',
    ...args
  })
}

const notifyInfo = (message = 'info', title = '提示', ...args) => {
  return Notification({
    title,
    message,
    type: 'info',
    ...args
  })
}

const notifyError = (message = 'error', title = '错误', ...args) => {
  return Notification({
    title,
    message,
    type: 'error',
    ...args
  })
}

export default {
  msgInfo,
  msgSuccess,
  msgWarn,
  msgError,
  msgHtml,
  msgConfirm,

  notifySuccess,
  notifyWarn,
  notifyInfo,
  notifyError
}
