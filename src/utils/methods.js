
// 创建特定长度的，数组
// class ArrarySpcifiLen {
//   constructor(len = 0, val = null) {
// this = new Array(len).fill(val, 0, len)
//   }
// }

// 复制内容
const doc = window.document
let copyElem
const copyContent = (content) => {
  if (!copyElem) {
    copyElem = doc.createElement('textarea')
    var styles = copyElem.style
    styles.width = '48px'
    styles.height = '24px'
    styles.position = 'fixed'
    styles.zIndex = '0'
    styles.left = '-500px'
    styles.top = '-500px'
    doc.body.appendChild(copyElem)
  }
  copyElem.value = content || ''
  copyElem.select()
  copyElem.setSelectionRange(0, copyElem.value.length)
  doc.execCommand('copy')
  copyElem.blur()
}

function deepClone(sourceObj) {
  if (sourceObj === null) return sourceObj
  if (sourceObj instanceof Date) return new Date(sourceObj)
  if (sourceObj instanceof RegExp) return new RegExp(sourceObj)
  if (typeof sourceObj !== 'object') return sourceObj
  var retObj = {}
  if (sourceObj instanceof Array) {
    retObj = []
    for (const key of sourceObj) {
      if (Object.prototype.hasOwnProperty.call(sourceObj, key)) {
        retObj[key] = deepClone(sourceObj[key])
      }
    }
  }
  for (const key in sourceObj) {
    if (Object.prototype.hasOwnProperty.call(sourceObj, key)) {
      retObj[key] = deepClone(sourceObj[key])
    }
  }
  return retObj
}

export {
  copyContent,
  deepClone
}
