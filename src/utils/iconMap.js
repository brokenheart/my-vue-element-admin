// export const iconMap = [
//   { reset: 'refresh' },
//   { edit: 'edit' },
//   { search: 'search' },
//   { plus: 'plus' },
//   { minus: 'minus' },
//   { check: 'check' },
//   { close: 'close' },
//   { delete: 'delete' },
//   { setting: 'setting' },
//   { more: 'more-outline' },
//   { star: 'star-off' },
//   { upload: 'upload2' },
//   { download: 'download' }

// ]

export const iconMap = {
  reset: { icon: 'refresh', title: '重置' },
  edit: { icon: 'edit', title: '编辑' },
  search: { icon: 'search', title: '搜索' },
  plus: { icon: 'plus', title: '新增' },
  minus: { icon: 'minus', title: '删减' },
  check: { icon: 'check', title: '通过' },
  close: { icon: 'close', title: '关闭' },
  delete: { icon: 'delete', title: '删除' },
  setting: { icon: 'setting', title: '设置' },
  more: { icon: 'more-outline', title: '更多' },
  star: { icon: 'star-off', title: '收藏' },
  upload: { icon: 'upload2', title: '上传' },
  download: { icon: 'download', title: '下载' },
  male: { icon: 'male', title: '男' },
  female: { icon: 'female', title: '女' }
}

const req = require.context('@/icons/svg', false, /\.svg$/)
const requireAll = requireContext => requireContext.keys()

const re = /\.\/(.*)\.svg/

export const svgIcons = requireAll(req).map(i => {
  return i.match(re)[1]
})

export const elementIconList = [
  'sugar',
  'ice-cream-round',
  'ice-cream-square',
  'lollipop',
  'potato-strips',
  'milk-tea',
  'ice-drink',
  'ice-tea',
  'coffee',
  'orange',
  'pear',
  'apple',
  'cherry',
  'watermelon',
  'grape',
  'refrigerator',
  'goblet-square-full',
  'goblet-square',
  'goblet-full',
  'goblet',
  'cold-drink',
  'coffee-cup',
  'water-cup',
  'hot-water',
  'ice-cream',
  'dessert',
  'tableware',
  'burger',
  'knife-fork',
  'fork-spoon',
  'chicken',
  'food',
  'dish-1',
  'dish',
  'moon-night',
  'moon',
  'cloudy-and-sunny',
  'partly-cloudy',
  'cloudy',
  'sunny',
  'sunset',
  'sunrise-1',
  'sunrise',
  'heavy-rain',
  'lightning',
  'light-rain',
  'wind-power',
  'baseball',
  'soccer',
  'football',
  'basketball',
  'ship',

  'truck',

  'bicycle',

  'mobile-phone',

  'service',

  'key',

  'unlock',

  'lock',

  'watch',

  'watch-1',

  'timer',

  'alarm-clock',

  'map-location',

  'delete-location',

  'add-location',

  'location-information',

  'location-outline',

  'location',

  'place',

  'discover',

  'first-aid-kit',

  'trophy-1',

  'trophy',

  'medal',

  'medal-1',

  'stopwatch',

  'mic',

  'copy-document',

  'full-screen',

  'switch-button',

  'aim',

  'crop',

  'odometer',

  'time',

  'bangzhu',

  'close-notification',

  'microphone',

  'turn-off-microphone',

  'position',

  'postcard',

  'message',

  'chat-line-square',

  'chat-dot-square',

  'chat-dot-round',

  'chat-square',

  'chat-line-round',

  'chat-round',

  'set-up',

  'turn-off',

  'open',

  'connection',

  'link',

  'cpu',

  'thumb',

  'female',

  'male',

  'guide',

  'news',

  'price-tag',

  'discount',

  'wallet',

  'coin',

  'money',

  'bank-card',

  'box',

  'present',

  'sell',

  'sold-out',

  'shopping-bag-2',

  'shopping-bag-1',

  'shopping-cart-2',

  'shopping-cart-1',

  'shopping-cart-full',

  'smoking',

  'no-smoking',

  'house',

  'table-lamp',

  'school',

  'office-building',

  'toilet-paper',

  'notebook-2',

  'notebook-1',

  'files',

  'collection',

  'receiving',

  'suitcase-1',

  'suitcase',

  'film',

  'collection-tag',

  'data-analysis',

  'pie-chart',

  'data-board',

  'data-line',

  'reading',

  'magic-stick',

  'coordinate',

  'mouse',

  'brush',

  'headset',

  'umbrella',

  'scissors',

  'mobile',

  'attract',

  'monitor',

  'search',

  'takeaway-box',

  'paperclip',

  'printer',

  'document-add',

  'document',

  'document-checked',

  'document-copy',

  'document-delete',

  'document-remove',

  'tickets',

  'folder-checked',

  'folder-delete',

  'folder-remove',

  'folder-add',

  'folder-opened',

  'folder',

  'edit-outline',

  'edit',

  'date',

  'c-scale-to-original',

  'view',

  'rank',

  'sort-down',

  'sort-up',

  'sort',

  'finished',

  'refresh-left',

  'refresh-right',

  'refresh',

  'video-play',

  'video-pause',

  'd-arrow-right',

  'd-arrow-left',

  'arrow-up',

  'arrow-down',

  'arrow-right',

  'arrow-left',

  'top-right',

  'top-left',

  'top',

  'bottom',

  'right',

  'back',

  'bottom-right',

  'bottom-left',

  'caret-top',
  'caret-bottom',
  'caret-right',
  'caret-left',
  'd-caret',
  'share',
  'menu',
  's-grid',
  's-check',
  's-data',
  's-opportunity',
  's-custom',
  's-claim',
  's-finance',
  's-comment',
  's-flag',
  's-marketing',
  's-shop',
  's-open',
  's-management',
  's-ticket',
  's-release',
  's-home',
  's-promotion',
  's-operation',
  's-unfold',
  's-fold',
  's-platform',
  's-order',
  's-cooperation',
  'bell',
  'message-solid',
  'video-camera',
  'video-camera-solid',
  'camera',
  'camera-solid',
  'download',
  'upload2',
  'upload',
  'picture-outline-round',
  'picture-outline',
  'picture',
  'close',
  'check',
  'plus',
  'minus',
  'help',
  's-help',
  'circle-close',
  'circle-check',
  'circle-plus-outline',
  'remove-outline',
  'zoom-out',
  'zoom-in',
  'error',
  'success',
  'circle-plus',
  'remove',
  'info',
  'question',
  'warning-outline',
  'warning',
  'goods',
  's-goods',
  'star-off',
  'star-on',
  'more-outline',
  'more',
  'phone-outline',
  'phone',
  'user',
  'user-solid',
  'setting',
  's-tools',
  'delete',
  'delete-solid',
  'eleme',
  'platform-eleme',
  'loading'
]
