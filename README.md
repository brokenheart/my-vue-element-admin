# vue-admin-template

## 模板来源以下项目

* [vue-element-admin](https://gitee.com/panjiachen/vue-element-admin)
* [vue-admin-template](https://gitee.com/panjiachen/vue-admin-template)

## 基于[element-ui](https://element.eleme.io/#/zh-CN/component/installation)做了以下组件的简单封装

* BoxLayout `上中下布局`
* MyButton 和[el-button](https://element.eleme.io/#/zh-CN/component/button)用法一样
* MyCascader 和[el-cascader](https://element.eleme.io/#/zh-CN/component/cascader)用法基本一样
* MyCheckbox 对[el-checkbox](https://element.eleme.io/#/zh-CN/component/checkbox)进行了封装
* MyDialog
* MyDrawer
* MyForm
* MyIcon
* MyLink
* MyPagination
* MyPopconfirm
* MyPopover
* MyRadio
* MyRate
* MySelect
* MyTable
* MyTabs
* MyTooltip

### 变动较大的有

- BoxLayout
- MyDialog
- MyDrawer
- MyForm
- MyPagination
- MyTable

## 基本展示

* ![基本展示页](./show-imgs/展示页.jpg)

* ![my-table与分页](./show-imgs/table与分页.jpg)

* ![全选与导出](./show-imgs/全选及导出.jpg)

* ![my-dialog](./show-imgs/mydialog弹窗.jpg)

* ![my-drawer橱窗](./show-imgs/橱窗.jpg)

* ![my-icon](./show-imgs/my-icon.jpg)